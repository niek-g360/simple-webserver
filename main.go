package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func exampleHandler(w http.ResponseWriter, r *http.Request) {
	h, err := os.Hostname()
	if err != nil {
		w.Write([]byte("Hello, World!"))
		return
	}
	w.Write([]byte(fmt.Sprintf("Hello, %s!", h)))
}

func main() {
	http.HandleFunc("/", exampleHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
