# Simple Webserver

A simple webserver running in a Docker container that greets the container's
hostname it's running in.

## Requirements

- [Go][1]
- [Docker][2]

## Running

First build the image:

```bash
docker build -t niek-g360/kubernetes-example:latest .
```

Then you can run a container of the image:

```bash
docker run -p 8080:8080 registry.gitlab.com/niek-g360/kubernetes-example:latest
```

## Usage

Once the server is running, either through the command line or Docker, you can
send HTTP requests to the server on port 8080:

```bash
$ curl localhost:8080
Hello, 4a0517ef5211!
```

[1]: https://golang.org/doc/install
[2]: https://docs.docker.com/get-docker/
