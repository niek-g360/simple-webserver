FROM golang:alpine AS builder

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go build -o app

FROM scratch

COPY --from=builder /go/src/app/app /app

CMD ["/app"]
